const addOrderAction = (orderItemsData) => {
    return {
        type: 'ADD_ORDER',
        payload: {
            ...orderItemsData
        }
    }
}

export default addOrderAction;