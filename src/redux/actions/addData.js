const addData = (formData) => {
    return {
        type: 'ADD_DATA',
        payload: {
            ...formData
        }
    }
}

export default addData;