const incrementData = (itemData) => {
    return {
        type: 'ADD_CART_DATA',
        payload: {
            ...itemData
        }
    }
}

export default incrementData;