const resetCount = () => {
    return {
        type: 'RESET_COUNT',
    }
}

export default resetCount;