const resetCart = () => {
    return {
        type: 'RESET_CART',
    }
}

export default resetCart;