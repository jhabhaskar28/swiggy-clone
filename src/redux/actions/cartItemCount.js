const cartItemCount = (count) => {
    return {
        type: 'STORE_COUNT',
        payload: {
            ...count
        }
    }
}

export default cartItemCount;