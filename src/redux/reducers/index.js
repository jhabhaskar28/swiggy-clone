import {combineReducers} from 'redux';
import storeData from './storeData';
import restaurantData from './restaurantData';
import cartData from './cartData';
import cartItemCountData from './cartItemCountData';
import addOrder from './addOrder';

const rootReducer = combineReducers({
    storeData,
    restaurantData,
    cartData,
    cartItemCountData,
    addOrder
});

export default rootReducer;