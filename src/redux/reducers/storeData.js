const initialState = {
    members: [],
};

const storeData = (state = initialState, action) => {
    switch(action.type){
        case 'ADD_DATA': return {
            ...state,
            members: [
                ...state.members,
                action.payload
            ]
        };
        default: return state;
    }
}

export default storeData;