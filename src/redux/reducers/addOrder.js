const initialState = {
    orders: [

    ]
}

const addOrder = (state = initialState, action) => {
    switch(action.type){
        case 'ADD_ORDER': return {
            ...state,
            orders: [
                ...state.orders,
                action.payload
            ]
        };
        default: return state;
    }
}

export default addOrder;