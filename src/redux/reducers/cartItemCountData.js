const initialState = {
    count: 0
}

const cartItemCountData = (state = initialState, action) => {
    switch(action.type){
        case 'STORE_COUNT': return {
            ...state,
            ...action.payload
        };
        case 'RESET_COUNT': return {
            ...state,
            count: 0
        };

        default: return state;
    }
}

export default cartItemCountData;