const initialState = {
    items: [

    ]
}

const cartData = (state = initialState, action) => {
    switch(action.type){
        case 'ADD_CART_DATA': return {
            ...state,
            items: [
                ...state.items,
                action.payload
            ]
        };
        
        case 'RESET_CART': return {
            ...state,
            items: []
        };

        default: return state;
    }
}

export default cartData;

