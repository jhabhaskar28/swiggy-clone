import React from "react";
import validator from 'validator';
import {connect} from 'react-redux';
import addData from '../redux/actions/addData';
import {Link} from 'react-router-dom';
import '../Signup.css';

class Signup extends React.Component{
    constructor(props){

        super(props);
    
        this.state = {
    
          phoneNumber: '',
          name: '',
          email: '',
          referralCode: '',
          password: '',
          passwordRepeat: '',
    
          isReferralAvailable: false,
          isNumberValid: true,
          isNameValid: true,
          isEmailValid: true,
          isPasswordValid: true,
          isPasswordRepeatValid: true,
          isContinueValidated: false,
          isSignUpValid: false
        };
    
    }

    handleContinue = () => {

        let numberValidFlag = true;
        let nameValidFlag = true;
        let emailValidFlag = true;

        if (!validator.isNumeric(this.state.phoneNumber) || this.state.phoneNumber.length < 10 || this.state.phoneNumber.length > 10) {
            numberValidFlag = false;
        }

        if (this.state.name.trim().length === 0 || /[0-9]/.test(this.state.name) === true || /[!@#$%^&*(),.?":{}|<>]/.test(this.state.name) === true) {
            nameValidFlag = false;
        }

        if (!validator.isEmail(this.state.email)) {
            emailValidFlag = false;
        }

        if (numberValidFlag && nameValidFlag && emailValidFlag) {

            this.setState({
                isNumberValid: numberValidFlag,
                isNameValid: nameValidFlag,
                isEmailValid: emailValidFlag,
                isContinueValidated: true
            });

        } else {

            this.setState({
                isNumberValid: numberValidFlag,
                isNameValid: nameValidFlag,
                isEmailValid: emailValidFlag,
                isContinueValidated: false
            });

        }

    }
    
    handleSignUp = () => {

        let passwordValidFlag = true;
        let passwordRepeatValidFlag = true;

        if (this.state.password.length < 6 || this.state.password.trim().length < 6) {
            passwordValidFlag = false;
        }

        if (this.state.passwordRepeat !== this.state.password) {
            passwordRepeatValidFlag = false;
        }

        if (passwordValidFlag && passwordRepeatValidFlag) {

            this.setState({
                isPasswordValid: passwordValidFlag,
                isPasswordRepeatValid: passwordRepeatValidFlag,
                isSignUpValid: true
            }, () => {
                const userData = {
                    name: this.state.name,
                    phoneNumber: this.state.phoneNumber,
                    email: this.state.email,
                    password: this.state.password
                };

                this.props.handleSignUpClick();
                this.props.addData(userData);
            });

        } else {

            this.setState({
                isPasswordValid: passwordValidFlag,
                isPasswordRepeatValid: passwordRepeatValidFlag,
                isSignUpValid: false
            });

        }
    }

    render(){
        return (
            <div className='signupMainPage'>
                <section className='signupCover'>
                    <img src='https://cdn.dribbble.com/users/1916450/screenshots/6178375/iphone_x_elements_4x.png' alt='phone'/>
                    {/* <img src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_768,h_978/pixel_wbdy4n' alt='phone'/> */}
                </section>
                <div className="signupContainer">
                    <Link to='/'><i className ="fa-sharp fa-solid fa-xmark"></i></Link>
                    <header className="signupHeader">
                        <div>
                            <h3>Sign up</h3>
                            <p><span className="underline">or </span><span className="loginText">login to your account</span></p>
                        </div>
                        <img src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto/Image-login_btpq7r' alt='food' />
                    </header>

                    <form className="signupForm">
                        {!this.state.isContinueValidated &&
                            <>
                                <section>
                                    <div className="form-floating">
                                        <input type="number" className="form-control inputFields" id="floatingInputNumber" placeholder="Phone number" value={this.state.phoneNumber} onChange={(event) => {
                                            this.setState({
                                                phoneNumber: event.target.value
                                            });
                                        }} />
                                        {this.state.isNumberValid && <label htmlFor="floatingInputNumber">Phone number</label>}
                                        {!this.state.isNumberValid && <label htmlFor="floatingInputNumber" className="formErrorMessage">Enter your phone number</label>}
                                    </div>
                                    <div className="form-floating">
                                        <input type="text" className="form-control inputFields" id="floatingInputName" placeholder="Name" value={this.state.name} onChange={(event) => {
                                            this.setState({
                                                name: event.target.value
                                            });
                                        }} />
                                        {this.state.isNameValid && <label htmlFor="floatingInputName">Name</label>}
                                        {!this.state.isNameValid && <label htmlFor="floatingInputName" className="formErrorMessage">Enter your name</label>}
                                    </div>
                                    <div className="form-floating">
                                        <input type="email" className="form-control inputFields" id="floatingInputEmail" placeholder="Email" value={this.state.email} onChange={(event) => {
                                            this.setState({
                                                email: event.target.value
                                            });
                                        }} />
                                        {this.state.isEmailValid && <label htmlFor="floatingInputEmail">Email</label>}
                                        {!this.state.isEmailValid && <label htmlFor="floatingInputEmail" className="formErrorMessage">Invalid email address</label>}
                                    </div>
                                    {this.state.isReferralAvailable &&
                                        <div className="form-floating">
                                            <input type="text" className="form-control inputFields" id="floatingInputReferral" placeholder="Referral Code" value={this.state.referralCode} onChange={(event) => {
                                                this.setState({
                                                    referralCode: event.target.value
                                                });
                                            }} />
                                            <label htmlFor="floatingInputReferral">Referral Code</label>
                                        </div>
                                    }
                                </section>
                                <section>
                                    {!this.state.isReferralAvailable && <p onClick={() => {
                                        this.setState({
                                            isReferralAvailable: true
                                        });
                                    }} className="referralCode">Have a referral code?</p>}
                                    <button type="button" className="btn form-control continueButton" onClick={this.handleContinue}>CONTINUE</button>
                                    <small>By creating an account, I accept the <span className='boldTAC'>Terms and Conditions & Privacy Policy</span></small>
                                </section>
                            </>
                        }
                        {this.state.isContinueValidated && !this.state.isSignUpValid &&
                            <>
                                <section>
                                    <div className="form-floating">
                                        <input type="password" className="form-control inputFields" id="floatingInputPassword" placeholder="Password" value={this.state.password} onChange={(event) => {
                                            this.setState({
                                                password: event.target.value
                                            });
                                        }} />
                                        {this.state.isPasswordValid && <label htmlFor="floatingInputPassword">Password (Minimum 6 characters)</label>}
                                        {!this.state.isPasswordValid && <label htmlFor="floatingInputPassword" className="formErrorMessage">Enter a password</label>}
                                    </div>
                                    <div className="form-floating">
                                        <input type="password" className="form-control inputFields" id="floatingInputRepeatPassword" placeholder="Password Repeat" value={this.state.passwordRepeat} onChange={(event) => {
                                            this.setState({
                                                passwordRepeat: event.target.value
                                            });
                                        }} />
                                        {this.state.isPasswordRepeatValid && <label htmlFor="floatingInputRepeatPassword">Repeat Password</label>}
                                        {!this.state.isPasswordRepeatValid && <label htmlFor="floatingInputRepeatPassword" className="formErrorMessage">Enter repeat password</label>}
                                    </div>
                                </section>
                                <button type="button" className="btn form-control continueButton" onClick={this.handleSignUp}>SIGN UP</button>
                                <small>By creating an account, I accept the <span className='boldTAC'>Terms and Conditions & Privacy Policy</span></small>
                            </>
                        }
                        {this.state.isSignUpValid &&
                            <>
                                <h2>Hello, <span className="loginName">{this.state.name}</span></h2>
                                <p>You are signed in successfully.</p>
                            </>
                        }

                    </form>
                </div>
            </div>
        );
    }
}


const mapDispatchToProps = (dispatch) => {
  return {
    addData: (payload) => {
      return dispatch(addData(payload));
    }
  }
}

export default connect(null,mapDispatchToProps)(Signup);