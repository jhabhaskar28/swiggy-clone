import React from 'react';
import '../Offers.css';

class Offers extends React.Component{
    render(){
        return(
            <div className='offersContainer'>
                <div className='offersHeader'>
                    <div>
                        <h1>Offers for you</h1>
                        <h4>Explore top deals and offers exclusively for you!</h4>
                    </div>
                    <img src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto/KHu24Gqw_md3ham' alt='logo' />
                </div>
                <div className='offersBody'>
                    <h3>Available Coupons</h3>
                    <div className='couponContainer'>
                        <h6>TRYNEW</h6>
                        <p>Get upto 60% off on selected restaurants.</p>
                        <small className='mutedText'>Terms and Conditions applicable.</small>
                    </div>
                </div>
            </div>
        );
    }
}

export default Offers;