import React from "react";
import IndividualOrders from "./IndividualOrders";
import '../Orders.css';
import {connect} from 'react-redux';

class Orders extends React.Component{

    render(){

        return (
            <>
                {this.props.orders.length>0 && <div className="ordersContainer">
                    <div className="orderPageTitle">
                        <h3>Your Past Orders :</h3>
                    </div>
                    <div className="singleOrdersContainer">
                        {this.props.orders.map((order) => {
                            return (
                                <div key={order.restaurantID}>
                                    <IndividualOrders order={order} />
                                </div>
                            )
                        })}
                    </div>
                </div>}
                {this.props.orders.length === 0 && 
                    <div className="noOrderScreen">
                        <h1>Oops! No orders yet...</h1>
                    </div>
                }
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return{
        orders: state.addOrder.orders
    }
}

export default connect(mapStateToProps,null)(Orders);