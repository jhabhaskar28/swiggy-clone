import React from "react";
import {Link} from 'react-router-dom';
import '../Search.css';

class Search extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            searchText: '',
        };
    }

    render(){
        // console.log(this.props.products);
        return (
            <div className="searchContainer">
                <form className="d-flex" role="search">
                    <input className="form-control searchBar" type="search" placeholder="Search for restaurants" aria-label="Search" value={this.state.searchText} onChange={(event) => {
                        this.setState({
                            searchText: event.target.value
                        });
                    }}/>
                </form>
                <div className="cuisines">
                    <h5>Popular Cuisines</h5>
                    <img src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto/rng/md/carousel/production/lbtzwnwla1pam1np4jtg' alt='food'/>
                    <img src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto/rng/md/carousel/production/kmvbd3hyswd147u4qdn1' alt='food'/>
                    <img src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto/rng/md/carousel/production/hvc4l0r0bgrtl6vdbbzv' alt='food'/>
                    <img src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto/rng/md/carousel/production/ee5ynhqgyhwdoukilzfu' alt='food'/>
                    <img src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto/rng/md/carousel/production/iwvt76wvh3a7dxmkljxd' alt='food'/>
                    <img src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto/rng/md/carousel/production/vntl1lutut9bqsxjninx' alt='food'/>
                    <img src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto/rng/md/carousel/production/wfzaxacltlxyi4shmm2u' alt='food'/>
                    <img src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto/rng/md/carousel/production/hk7gdfeiwmy5nx6prv97' alt='food'/>
                </div>
                    {this.state.searchText !== '' && this.props.products.filter((product) => {
                        let searchedTextLower = this.state.searchText.toLowerCase();
                        let productNameLower = product.name.toLowerCase();

                        return productNameLower.includes(searchedTextLower);
                        })
                        .map((product) => {
                            return (
                                <Link to={`restaurant/${product.id}`} key={product.id}>
                                    <div className="searchedRestaurant">
                                        <img src={product.img_url} alt='food' />
                                        <div className='searchedRestaurantDetails'>
                                            <h5>{product.name}</h5>
                                            <h6 className="mutedText">Restaurant</h6>
                                        </div>
                                    </div>
                                </Link>
                            )
                        })
                    }
            </div>
        );
    }
}

export default Search;