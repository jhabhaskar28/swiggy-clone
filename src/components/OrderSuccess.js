import React from "react";
import '../OrderSuccess.css';

class OrderSuccess extends React.Component{
    render(){
        return(
            <div className="container successPage">
                <img src="https://neetable.com/img/blog/blog-inner/build-an-on-demand-food-delivery-app-like-swiggy/how-to-build-an-on-demand-food-delivery-app-like-swiggy.jpg" alt='success'></img>
                <div className="orderMessage">
                    <div className="orderMessageImage">
                        <h1>YAY! Your order is successful</h1>
                        <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Eo_circle_orange_checkmark.svg/768px-Eo_circle_orange_checkmark.svg.png' alt="logo" />
                    </div>
                    <h5 className="mutedText">Hold tight. Our delivery partner will be arriving with your order shortly.</h5>
                </div>
            </div>
        );
    }
}

export default OrderSuccess;
