import React from "react";
import {Link} from 'react-router-dom';
import '../Restaurant.css';

class Restaurant extends React.Component{
    render(){
        return(
            <Link to={`restaurant/${this.props.id}`}>
                <div className="restaurant">
                    <div className="restaurantImage">
                        <img src={this.props.image} alt='food'></img>
                    </div>
                    <div className="restaurantDetails">
                        <h6 className="restaurantName">{this.props.name}</h6>
                        <p>{this.props.cuisines}</p>
                        <div className="ratingAndCostDetails">
                        <p className="rating"><i className="fa-solid fa-star"></i> {this.props.rating}</p>
                        <p>.</p>
                        <p>{this.props.time} MINS</p>
                        <p>.</p>
                        <p><i className="fa-solid fa-indian-rupee-sign rupeeSign"></i>{this.props.cost} FOR TWO</p>
                        </div>
                    </div>
                    <small className='discount'><i className="fa-solid fa-tag"></i> {this.props.sale}% off | Use TRYNEW</small>
                </div>
            </Link>
        );
    }
}

export default Restaurant;


