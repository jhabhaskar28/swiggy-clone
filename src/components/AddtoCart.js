import React from "react";
import {connect} from 'react-redux';
import incrementData from "../redux/actions/incrementData";
import '../AddtoCart.css';
import cartItemCount from '../redux/actions/cartItemCount';

class AddtoCart extends React.Component{
    constructor(){
        super();

        this.state = {
            noOfItems: 0,
            isItemsUpdated: false
        }

        this.selectedItemsFromRestaurant = [];
        this.cartRestaurant = [];
    }

    handleIncrement = () => {
        this.setState((previousState) => {
            return {
                noOfItems: previousState.noOfItems + 1,
                isItemsUpdated: true
            }
        },this.updateStore);
    }

    handleDecrement = () => {

        if(this.state.noOfItems > 0){
            this.setState((previousState) => {
                return {
                    noOfItems: previousState.noOfItems - 1,
                    isItemsUpdated: true
                }
            },this.updateStore);
        }
    }

    updateStore = () => {

        const itemData = {
            itemID: `${this.props.itemID}&${this.props.restaurantID}`,
            noOfItems: this.state.noOfItems
        }

        this.props.incrementData(itemData);
    }

    componentDidMount(){
        if(this.props.items.length > 0 && this.cartRestaurant[0].id===this.props.restaurantID && this.selectedItemsFromRestaurant[this.props.itemID]!==undefined){
            this.setState({
                noOfItems: this.selectedItemsFromRestaurant[this.props.itemID]
            });
        }
    }


    render(){

        if(this.props.items.length > 0){

            // console.log("Propss",this.props.items);
            let lastestRestaurant = this.props.items[this.props.items.length-1];
            // console.log(lastestRes);
            let restaurantIDs = parseInt(lastestRestaurant.itemID.split('&')[1]);
            // console.log(restaurantIDss);
    
            this.cartRestaurant = this.props.restaurants.filter((restaurant) => {
                return parseInt(restaurant.id) === restaurantIDs;
            });
    
            // console.log("CRs",cartRestaurants[0]);
    
            this.selectedItemsFromRestaurant = this.props.items.reduce((accumulator,currentValue) => {
                if(parseInt(currentValue.itemID.split('&')[1]) === restaurantIDs){
                    if(accumulator[currentValue.itemID.split('&')[0]] === undefined){
                        accumulator[currentValue.itemID.split('&')[0]] = currentValue.noOfItems;
                    } else {
                        accumulator[currentValue.itemID.split('&')[0]] = currentValue.noOfItems;
                    }
                }
                return accumulator;
            },{});

            // console.log("Add",this.selectedItemsFromRestaurant);
            let totalItems = Object.keys(this.selectedItemsFromRestaurant)
            .reduce((total,currentValue) => {
                total += this.selectedItemsFromRestaurant[currentValue];
                return total;
            },0);

            this.props.cartItemCount({count: totalItems});
    
            // console.log("Cart",selectedItemsFromRestaurant);
        }

        return (
            <div className="cartButtonContainer">
                <button onClick={this.handleDecrement}>-</button>
                    <label>{this.state.noOfItems}</label>
                <button onClick={this.handleIncrement}>+</button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.cartData.items,
        restaurants: state.restaurantData.restaurants,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        incrementData: (payload) => {
            return dispatch(incrementData(payload));
        },
        cartItemCount: (payload) => {
            return dispatch(cartItemCount(payload));
        }
    }
}

// export default AddtoCart;

export default connect(mapStateToProps,mapDispatchToProps)(AddtoCart);