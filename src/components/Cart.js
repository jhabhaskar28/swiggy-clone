import React from 'react';
import {connect} from 'react-redux';
import AddtoCart from './AddtoCart';
import {Link} from 'react-router-dom';
import cartItemCount from '../redux/actions/cartItemCount';
import resetCart from '../redux/actions/resetCart';
import resetCount from '../redux/actions/resetCount';
import addOrderAction from '../redux/actions/addOrderAction';
import '../Cart.css';

class Cart extends React.Component{
    render(){
        console.log("Logged",this.props.isUserLoggedIn);
        
        let totalPriceOfItems = 0;
        let selectedItems = [];
        let selectedItemsFromRestaurant = [];
        let cartRestaurant = [];

        if(this.props.items.length > 0){

            // console.log("Propss",this.props.items);
            let lastestRestaurant = this.props.items[this.props.items.length-1];
            // console.log(lastestRes);
            let restaurantIDs = parseInt(lastestRestaurant.itemID.split('&')[1]);
            // console.log(restaurantIDss);
    
            cartRestaurant = this.props.restaurants.filter((restaurant) => {
                return parseInt(restaurant.id) === restaurantIDs;
            });
    
            // console.log("CRs",cartRestaurants[0]);
    
            selectedItemsFromRestaurant = this.props.items.reduce((accumulator,currentValue) => {
                if(parseInt(currentValue.itemID.split('&')[1]) === restaurantIDs){
                    if(accumulator[currentValue.itemID.split('&')[0]] === undefined){
                        accumulator[currentValue.itemID.split('&')[0]] = currentValue.noOfItems;
                    } else {
                        accumulator[currentValue.itemID.split('&')[0]] = currentValue.noOfItems;
                    }
                }
                return accumulator;
            },{});

            Object.keys(selectedItemsFromRestaurant)
            .map((item) => {
                if(selectedItemsFromRestaurant[item] === 0){
                    delete selectedItemsFromRestaurant[item];
                }
                return '';
            });

            let totalItems = Object.keys(selectedItemsFromRestaurant)
            .reduce((total,currentValue) => {
                total += selectedItemsFromRestaurant[currentValue];
                return total;
            },0);

            this.props.cartItemCount({count: totalItems});
            // console.log("Cart",selectedItemsFromRestaurant);
    
    
            selectedItems = cartRestaurant[0].items.filter((item) => {
                return selectedItemsFromRestaurant[item.id] !== undefined && item;
            });
    
            totalPriceOfItems = selectedItems.reduce((totalPrice,currentValue) => {
                totalPrice += currentValue.price * selectedItemsFromRestaurant[currentValue.id].toFixed(2) ;
                return totalPrice;
            },0);
    
            totalPriceOfItems = totalPriceOfItems.toFixed(2);
            // console.log("SI",selectedItems);
            // console.log("Cart-SIR",selectedItemsFromRestaurant);
            // console.log("CR",cartRestaurant);
        }

        // console.log(selectedItems);

        return(
            <div className="cartContainer">
                { selectedItems.length > 0 && 
                <div className="singleRestaurantData">
                    <img src={cartRestaurant[0].img_url} alt='food'></img>
                    <div className="singleDetails">
                        <h3>{cartRestaurant[0].name}</h3>
                        <h6 className="mutedText">{cartRestaurant[0].cuisines.join()}</h6>
                        <h6><i className="fa-solid fa-tag"></i> {cartRestaurant[0].sale}% off | Use TRYNEW</h6>
                        <div className="ratingsAndPrice">
                            <div>
                                <p><i className="fa-solid fa-star"></i> {cartRestaurant[0].rating}</p>
                                <small className="mutedText">100+ ratings</small>
                            </div>
                            <p>|</p>
                            <div>
                                <p>{cartRestaurant[0].average_time} MINS</p>
                                <small className="mutedText">Delivery time</small>
                            </div>
                            <p>|</p>
                            <div>
                                <p><i className="fa-solid fa-indian-rupee-sign rupeeSign"></i>{cartRestaurant[0].average_cost} FOR TWO</p>
                                <small className="mutedText">Cost for two</small>
                            </div>
                        </div>
                    </div>
                </div>
                }
                {selectedItems.length > 0 && selectedItems.map((selectedItem) => {
                    return (
                        <div key={selectedItem.id} className="cartItem">
                            <div className='itemDetails'>
                                <img src={selectedItem.img_url} alt='food'/>
                                <div className='itemDetailsName'>
                                    <h6>
                                    {!selectedItem.veg && <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Non_veg_symbol.svg/2048px-Non_veg_symbol.svg.png' alt='sign'/>}
                                    {selectedItem.veg && <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Veg_symbol.svg/1200px-Veg_symbol.svg.png' alt='sign'/>}
                                    {selectedItem.name}
                                    </h6>
                                    <p>Qty: {selectedItemsFromRestaurant[selectedItem.id]}</p>
                                    <div className='addToCartButton'>
                                        <AddtoCart restaurantID={cartRestaurant[0].id} itemID={selectedItem.id}/>
                                    </div>
                                </div>
                            </div>
                            <h5>Price: <i className="fa-solid fa-indian-rupee-sign rupeeSign"></i>{(selectedItem.price * selectedItemsFromRestaurant[selectedItem.id]).toFixed(2)}</h5>
                        </div>
                    )
                })}
                { selectedItems.length > 0 && 
                <div className='checkoutContainer'>
                    <h4>Total: <i className="fa-solid fa-indian-rupee-sign rupeeSign"></i>{totalPriceOfItems}</h4>
                    <Link to={`/restaurant/${cartRestaurant[0].id}`}><button type="button" className="btn btn-success">Add More Items</button></Link>
                    {this.props.isUserLoggedIn && <Link to='/orderSuccess'><button type="button" className="btn btn-success" onClick={() => {
                        const orderItemsData = {
                            restaurantID: cartRestaurant[0].id,
                            slecetedItemsQty: selectedItemsFromRestaurant
                        };
                        this.props.addOrderAction(orderItemsData);
                        this.props.resetCart();
                        this.props.resetCount();
                    }}>Checkout</button></Link>}
                    {!this.props.isUserLoggedIn && 
                        <Link to='/signup'><button type="button" className="btn btn-success">Sign up to continue</button></Link>
                    }
                </div>
                }
                {selectedItems.length === 0 && 
                    <div className='emptyCart'>
                        <img src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_480/Cart_empty_-_menu_2x_ejjkf2' alt='empty'/>
                        <h5>Your cart is empty</h5>
                        <p className='mutedText'>You can go to home page to view more restaurants</p>
                        <Link to='/'><button type="button" className="btn btn-success">Take me home</button></Link>
                    </div>
                }
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        cartItemCount: (payload) => {
            return dispatch(cartItemCount(payload));
        },
        resetCart: () => {
            return dispatch(resetCart());
        },
        resetCount: () => {
            return dispatch(resetCount());
        },
        addOrderAction: (payload) => {
            return dispatch(addOrderAction(payload));
        }
    }
}

const mapStateToProps = (state) => {
    // console.log(state);
    return {
        items: state.cartData.items,
        restaurants: state.restaurantData.restaurants,
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Cart);