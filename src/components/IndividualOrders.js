import React from "react";
import {connect} from 'react-redux';

class IndividualOrders extends React.Component{
    render(){
        
        let restaurantID = this.props.order.restaurantID;
        let restaurant = this.props.restaurants.find((restaurant) => {
            return parseInt(restaurant.id) === parseInt(restaurantID);
        });

        let selectedItems = restaurant.items.filter((item) => {
            return this.props.order.slecetedItemsQty[item.id] !== undefined && item;
        });

        return (
            <div>
                <div className="singleRestaurantData">
                    <img src={restaurant.img_url} alt='food'></img>
                    <div className="singleDetails">
                        <h3>{restaurant.name}</h3>
                        <h6 className="mutedText">{restaurant.cuisines.join()}</h6>
                        <h6><i className="fa-solid fa-tag"></i> {restaurant.sale}% off | Use TRYNEW</h6>
                        <div className="ratingsAndPrice">
                            <div>
                                <p><i className="fa-solid fa-star"></i> {restaurant.rating}</p>
                                <small className="mutedText">100+ ratings</small>
                            </div>
                            <p>|</p>
                            <div>
                                <p>{restaurant.average_time} MINS</p>
                                <small className="mutedText">Delivery time</small>
                            </div>
                            <p>|</p>
                            <div>
                                <p><i className="fa-solid fa-indian-rupee-sign rupeeSign"></i>{restaurant.average_cost} FOR TWO</p>
                                <small className="mutedText">Cost for two</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div className="orderItems">
                        {selectedItems.map((item) => {
                            return (
                                <div className="orderItem">
                                    <img src={item.img_url} alt='food'></img>
                                    <div className="orderItemDetails">
                                        <h5>{item.name}</h5>
                                        <h5>Qty: {this.props.order.slecetedItemsQty[item.id]}</h5>
                                        <h5 className="mutedText">Price: {(item.price * this.props.order.slecetedItemsQty[item.id]).toFixed(2)}</h5>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        restaurants: state.restaurantData.restaurants,
    }
}

export default connect(mapStateToProps,null)(IndividualOrders);