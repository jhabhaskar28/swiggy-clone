import React from "react";
import '../SingleRestaurant.css';
import {Link} from 'react-router-dom';
import AddtoCart from "./AddtoCart";

class SingleRestaurant extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            isVegFilter: false,
            searchText: ''
        };
    }

    handleCheck = () => {
        let toggleFilter = this.state.isVegFilter ? false : true;

        this.setState({
            isVegFilter: toggleFilter
        });
    }

    render(){
        // console.log("restaurant");
        let restaurant = this.props.products.filter((product) => {
            return product.id === parseInt(this.props.routeProps.match.params.id);
        });
        // console.log(restaurant[0].items);
        return(
            <div className="singleRestaurantContainer">
                <div className="singleRestaurantData">
                    <img src={restaurant[0].img_url} alt='food'></img>
                    <div className="singleDetails">
                        <h3>{restaurant[0].name}</h3>
                        <h6 className="mutedText">{restaurant[0].cuisines.join()}</h6>
                        <h6><i className="fa-solid fa-tag"></i> {restaurant[0].sale}% off | Use TRYNEW</h6>
                        <div className="ratingsAndPrice">
                            <div>
                                <p><i className="fa-solid fa-star"></i> {restaurant[0].rating}</p>
                                <small className="mutedText">100+ ratings</small>
                            </div>
                            <p>|</p>
                            <div>
                                <p>{restaurant[0].average_time} MINS</p>
                                <small className="mutedText">Delivery time</small>
                            </div>
                            <p>|</p>
                            <div>
                                <p><i className="fa-solid fa-indian-rupee-sign rupeeSign"></i>{restaurant[0].average_cost} FOR TWO</p>
                                <small className="mutedText">Cost for two</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="restaurantItemsFilter">
                    <div className="form-check filterCheckBox">
                        <input className="form-check-input" type="checkbox" value="" id="defaultCheck1" onChange={this.handleCheck}/>
                            <label className="form-check-label" htmlFor="defaultCheck1">
                                Veg Only
                            </label>
                    </div>
                    <form className="d-flex" role="search">
                        <input className="form-control itemSearch" type="search" placeholder="Search for dishes..." aria-label="Search" value={this.state.searchText} onChange={(event) => {
                            this.setState({
                                searchText: event.target.value
                            });
                        }}/>
                    </form>
                </div>
                <div className="singleRestaurantItemsContainer">

                    <div className="singleRestaurantItems">
                        {!this.state.isVegFilter && restaurant[0].items.filter((item) => {
                            if(this.state.searchText === ''){
                                return item;
                            } else {
                                let searchTextLower = this.state.searchText.toLowerCase();
                                let itemNameLower = item.name.toLowerCase();

                                return itemNameLower.includes(searchTextLower);
                            }
                        })
                        .map((item) => {
                            return (
                                <div key={item.id} className='itemInRestaurant'>
                                    <div className='itemNameAndPrice'>
                                        {item.veg && 
                                            <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Veg_symbol.svg/1200px-Veg_symbol.svg.png' alt='sign'/>
                                        }
                                        {!item.veg && 
                                            <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Non_veg_symbol.svg/2048px-Non_veg_symbol.svg.png' alt='sign'/>
                                        }
                                        <h6>{item.name}</h6>
                                        <p><i className="fa-solid fa-indian-rupee-sign"></i> {item.price}</p>
                                    </div>
                                    <div className="addButtonContainer">
                                        <img src={item.img_url} alt='food'></img>
                                        <AddtoCart restaurantID={restaurant[0].id} itemID={item.id}/>
                                    </div>
                                </div>
                            )
                        })}
                        {this.state.isVegFilter && restaurant[0].items.filter((item) => {
                            if(this.state.searchText === ''){
                                return item.veg === true;
                            } else {
                                let searchTextLower = this.state.searchText.toLowerCase();
                                let itemNameLower = item.name.toLowerCase();

                                return item.veg && itemNameLower.includes(searchTextLower);
                            }
                            
                        })
                        .map((item) => {
                            return (
                                <div key={item.id} className='itemInRestaurant'>
                                    <div className='itemNameAndPrice'>
                                        {item.veg && 
                                            <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Veg_symbol.svg/1200px-Veg_symbol.svg.png' alt='sign'/>
                                        }
                                        {!item.veg && 
                                            <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Non_veg_symbol.svg/2048px-Non_veg_symbol.svg.png' alt='sign'/>
                                        }
                                        <h6>{item.name}</h6>
                                        <p><i className="fa-solid fa-indian-rupee-sign"></i> {item.price}</p>
                                    </div>
                                    <div className="addButtonContainer">
                                        <img src={item.img_url} alt='food'></img>
                                        <AddtoCart restaurantID={restaurant[0].id} itemID={item.id}/>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                    <div className="cartLogoContainer">
                        <img src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_480/Cart_empty_-_menu_2x_ejjkf2' alt='cart'/>
                        <p className="mutedText">Good food is always cooking!</p>
                        <p className="mutedText">Go ahead, order some yummy items from the menu.</p>
                        <Link to='/Cart'><button type="button" className="btn btn-success">Go to Cart</button></Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default SingleRestaurant;