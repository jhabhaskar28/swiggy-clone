import React from 'react';
import {BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom';
import Signup from './components/Signup';
import Restaurant from './components/Restaurant';
import './App.css';
import {connect} from 'react-redux';
import OrderSuccess from './components/OrderSuccess';
import Search from './components/Search';
import SingleRestaurant from './components/SingleRestaurant';
import resetCart from './redux/actions/resetCart';
import resetCount from './redux/actions/resetCount';
import Cart from './components/Cart';
import Help from './components/Help';
import Offers from './components/Offers';
import Orders from './components/Orders';

class App extends React.Component{
  constructor(){
    super();

    this.state = {
      isUserLoggedIn: false
    }
  }

  handleSignUpClick = () => {
    let toggleLogIn = this.state.isUserLoggedIn===true ? false : true;

    this.setState({
      isUserLoggedIn: toggleLogIn
    });
  }

  render(){
    
    // console.log(this.props.users.length);
    return(
        <Router>
          <Switch>
            <Route path='/signup' exact>
              <Signup handleSignUpClick={this.handleSignUpClick}/>
            </Route>
            <Route path='/'>
              <div className='mainPageContainer'>
                <ul className="nav">
                  <li className="nav-item">
                    <Link className="nav-link" to='/'><img src='https://upload.wikimedia.org/wikipedia/en/thumb/1/12/Swiggy_logo.svg/1280px-Swiggy_logo.svg.png' alt='logo'/></Link>
                  </li>
                  <div className='navbarItems'>
                    <li className="nav-item">
                      <Link className="nav-link" to='/search'><i className="fa-solid fa-magnifying-glass"></i> Search</Link>
                    </li>
                    <li className="nav-item">
                      <Link className="nav-link" to='/offers'><i className="fa-solid fa-percent"></i> Offers</Link>
                    </li>
                    <li className="nav-item">
                      <Link className="nav-link" to='/help'><i className="fa-regular fa-circle-question"></i> Help</Link>
                    </li>
                    {!this.state.isUserLoggedIn && 
                      <li className="nav-item">
                        <Link className="nav-link" to='/signup'><i className="fa-regular fa-user"></i> Sign In</Link>
                      </li>
                    }
                    {this.state.isUserLoggedIn && 
                      <div className="dropdown">
                        <button className="btn btn-success dropdown-toggle userButton" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                          {this.props.users[this.props.users.length-1].name}
                        </button>
                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                          <Link to='/cart'><li className="dropdown-item signoutButton">Cart</li></Link>
                          <Link to='/orders'><li className="dropdown-item signoutButton">Your Orders</li></Link>
                          <li className="dropdown-item signoutButton" onClick={() => {
                            this.handleSignUpClick();
                            this.props.resetCart();
                            this.props.resetCount();
                          }}>Sign Out</li>
                        </ul>
                      </div>
                    }
                    <li className="nav-item">
                      <Link className="nav-link" to='/cart'><i className="fa-solid fa-cart-arrow-down"></i> Cart ({this.props.cartItemCount})</Link>
                    </li>
                  </div>
                </ul>
                <Route path='/' exact>
                <div className="container-fluid banners">
                  <img src="https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_520,h_520/rng/md/carousel/production/pneknawbadtvceqzwiep" className="img-fluid" alt="Responsive"></img>
                  <img src="https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_520,h_520/rng/md/carousel/production/zpkkdkmvlj5cuvqbc50t" className="img-fluid" alt="Responsive"></img>
                  <img src="https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_520,h_520/rng/md/carousel/production/awurei8ypqkafoqay9ym" className="img-fluid" alt="Responsive"></img>
                  <img src="https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_520,h_520/rng/md/carousel/production/s5ug2key6e2sptaxku5v" className="img-fluid" alt="Responsive"></img>
                </div>
                <div className='container restaurantCount'>
                  <h3>16 Restaurants</h3>
                </div>
                <div className="container">
                  <div className="row">
                    {this.props.restaurants.map((restaurant) => {
                    return <div className="col-3" key={restaurant.id}>
                      <Restaurant id={restaurant.id} name={restaurant.name} image={restaurant.img_url} rating={restaurant.rating} cost={restaurant.average_cost} time={restaurant.average_time} cuisines={restaurant.cuisines.join()} sale={restaurant.sale}/>
                    </div> 
                    })}
                  </div>
                </div>
                </Route>
                <Route path='/restaurant/:id' exact render={(routeProps) => {
                    return <SingleRestaurant routeProps={routeProps} products={this.props.restaurants}/>
                  }} />
                <Route path='/search' exact><Search products={this.props.restaurants}/></Route>
                <Route path='/cart' exact><Cart isUserLoggedIn={this.state.isUserLoggedIn}/></Route>
                <Route path='/orderSuccess'><OrderSuccess /></Route>
                <Route path='/orders' exact><Orders/></Route>
                <Route path='/help' exact><Help /></Route>
                <Route path='/offers' exact><Offers /></Route>
                <footer className="footer">
                  <div className="footerContainer">
                    <div>
                      <h6 className="subHeader">COMPANY</h6>
                      <h6>About Us</h6>
                      <h6>Team</h6>
                      <h6>Careers</h6>
                      <h6>About Us</h6>
                      <h6>Swiggy Blog</h6>
                      <h6>Bug Bounty</h6>
                      <h6>Swiggy One</h6>
                      <h6>Swiggy Corporate</h6>
                      <h6>Swiggy Instamart</h6>
                      <h6>Swiggy Genie</h6>
                    </div>
                    <div>
                      <h6 className="subHeader">CONTACT</h6>
                      <h6>Help & Support</h6>
                      <h6>Partner with us</h6>
                      <h6>Ride with us</h6>
                    </div>
                    <div>
                      <h6 className="subHeader">LEGAL</h6>
                      <h6>Terms & Conditions</h6>
                      <h6>Refund & Cancellation</h6>
                      <h6>Privacy Policy</h6>
                      <h6>Cookie Policy</h6>
                      <h6>Offer Terms</h6>
                      <h6>Phishing & Fraud</h6>
                      <h6>Corporate - Swiggy Money Codes</h6>
                      <h6>Corporate - Swiggy Discount Voucher</h6>
                    </div>
                  </div>
                  <div className='finalFooter'>
                    <img src='https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_284/Logo_f5xzza' alt='logo'/>
                    <h5><i className="fa-regular fa-copyright"></i> 2023 Swiggy</h5>
                    <div className="smLogos">
                      <h5><i className="fa-brands fa-facebook"></i></h5>
                      <h5><i className="fa-brands fa-pinterest"></i></h5>
                      <h5><i className="fa-brands fa-instagram"></i></h5>
                      <h5><i className="fa-brands fa-twitter"></i></h5>
                    </div>
                  </div>
                </footer>
              </div>
            </Route>
          </Switch>
        </Router>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      resetCart: () => {
          return dispatch(resetCart());
      },
      resetCount: () => {
          return dispatch(resetCount());
      }
  }
}

const mapStateToProps = (state) => {
  return {
      restaurants: state.restaurantData.restaurants,
      users: state.storeData.members,
      cartItemCount: state.cartItemCountData.count
  }
} 

export default connect(mapStateToProps,mapDispatchToProps)(App);
